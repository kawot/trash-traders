﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class ItemView : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI label = default;
    public RectTransform pic => image.rectTransform;
    public Image img => image;
    [SerializeField] Image image = default;
    public Image HL => hlImage;
    [SerializeField] Image hlImage = default;
    public ParticleSystem matchEffect;

    public int id;
    public bool isEmpty = true;

    public void Init(Item item)
    {
        id = item.id;
        //label.text = item.caption;
        label.text = "";
        image.sprite = item.sprite;
        image.color = Color.white;
        image.rectTransform.anchoredPosition = Vector2.zero;
        hlImage.enabled = false;
        isEmpty = false;

        GetComponent<RectTransform>().DOAnchorPosY(10f, 2f).SetRelative(true).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo).SetDelay(Random.Range(0f, 2f));
    }

    public void OnTap()
    {
        if (isEmpty) return;
        if (!GameController.isReady) return;
        if (GameController.isHomeless != GameController.homelessTurn) return;

        switch (GameController.OnItemTap(id))
        {
            case ItemState.Normal:
                hlImage.enabled = false;
                break;
            case ItemState.Off:
                TurnOff();
                break;
            case ItemState.Chosen:
                hlImage.enabled = true;
                break;
            case ItemState.Skip:
                break;
            default:
                break;
        }
    }

    public void TurnOff()
    {
        hlImage.enabled = false;
        label.text = "";
        //image.sprite = null;
        hlImage.enabled = false;
        isEmpty = true;
        image.color = Color.clear;
    }
}

public enum ItemState
{
    Normal,
    Off,
    Chosen,
    Skip
}
