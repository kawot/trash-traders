﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemList", menuName = "Item List", order = 1)]
public class ItemList : ScriptableObject
{
    public List<Item> items;

    public void Init()
    {
        for (int i = 0; i < items.Count; i++)
        {
            items[i].id = i;
        }
    }
}
