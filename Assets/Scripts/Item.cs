﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Item", order = 1)]
public class Item : ScriptableObject
{
    [HideInInspector] public int id;
    public string caption;
    public Sprite sprite;
    public Item[] connects;
}
