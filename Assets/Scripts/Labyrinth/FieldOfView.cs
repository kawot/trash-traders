﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class FieldOfView : MonoBehaviour
{
    public Transform target;

    [SerializeField] int segments = 32;
    [SerializeField] float viewRadius = 10f;
    [SerializeField] LayerMask obstacleMask = default;


    private Vector3[] vertices;
    private Vector2[] uvs;
    private int[] triangles;
    private Mesh mesh; 
    private MeshFilter meshFilter; 

    // Start is called before the first frame update
    void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        mesh = new Mesh();
        mesh.name = "FoW Mesh";
        meshFilter.mesh = mesh;
    }

    private void FixedUpdate()
    {
        transform.position = target.position;
    }

    // Update is called once per frame
    void Update()
    {
        var viewPoints = new List<Vector3>();
        for (int i = 0; i <= segments; i++)
        {
            var angle = 360f * i / segments + target.eulerAngles.y;
            var newCheck = Check(angle);
            viewPoints.Add(newCheck.point - transform.position);
        }

        vertices = new Vector3[viewPoints.Count + 1];
        uvs = new Vector2[vertices.Length];
        triangles = new int[3 * (vertices.Length - 2)];

        vertices[0] = Vector3.zero;
        for (int i = 0; i <= vertices.Length - 2; i++)
        {
            vertices[i + 1] = viewPoints[i];
            var dir = viewPoints[i].normalized;
            uvs[i + 1] = new Vector2(dir.x, dir.z);
            if (i < vertices.Length - 2)
            {
                triangles[i * 3 + 0] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        mesh.Clear();

        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
    }

    ViewCastInfo Check(float angle)
    {
        var dir = DirFromAngle(angle);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, dir, out hit, viewRadius, obstacleMask))
        {
            return new ViewCastInfo(true, hit.point, hit.distance, angle);
        }
        else
        {
            return new ViewCastInfo(false, transform.position + dir * viewRadius, viewRadius, angle);
        }
    }

    Vector3 DirFromAngle(float angle)
    {
        return new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), 0, Mathf.Cos(angle * Mathf.Deg2Rad));
    }

    private struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }
}
