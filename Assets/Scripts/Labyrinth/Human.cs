﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Human : MonoBehaviour
{
    public static Vector3 mousePos;

    [SerializeField] float moveSpeed = 1f;

    private Camera cam;
    private Rigidbody body;
    //private Vector3 velocity;

    void Start()
    {
        cam = Camera.main;
        body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.y = 0f;

        TurnInput();
        MoveInput();
    }



    private void TurnInput()
    {
        transform.LookAt(mousePos);
    }

    private void MoveInput()
    {
        body.velocity = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical")) * moveSpeed;
    }

    //private void FixedUpdate()
    //{
    //    Move();
    //}

    //private void Move()
    //{
    //    transform.position += velocity * Time.fixedDeltaTime;
    //}
}
