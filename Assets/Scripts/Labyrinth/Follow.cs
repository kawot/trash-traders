﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    [SerializeField] Transform target = default;
    [SerializeField] float smooth = 0f;
    [Range(0f, 1f)] [SerializeField]  float mouseBias = 0.1f;
    [SerializeField] float mouseRange = 10f;

    private Vector3 currentVelocity;
    
    void FixedUpdate()
    {
        var mousePos = Human.mousePos;
        var dir = mousePos - target.position;
        if (dir.magnitude > mouseRange)
        {
            dir = dir.normalized * mouseRange;
            mousePos = target.position + dir;
        } 
        var targetPos = Vector3.Lerp(target.position, mousePos, mouseBias); 
        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref currentVelocity, smooth);
    }
}
