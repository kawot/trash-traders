﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class UI : MonoBehaviour
{
    public static UI instance;

    [SerializeField] GameObject lobbyUI = default;
    [SerializeField] Button multiplayerButton = default;
    //[SerializeField] Button singleButton = default;
    [SerializeField] GameObject howtoplayUI = default;
    [SerializeField] GameObject searchUI = default;
    [SerializeField] GameObject roomUI = default;
    [SerializeField] GameObject hostUI = default;
    [SerializeField] Button startButton = default;
    [SerializeField] Button soloButton = default;
    [SerializeField] GameUI gameUI = default;
    [SerializeField] Image warningDot = default;
    [SerializeField] private TextMeshProUGUI networkStatusLabel = default;
    [SerializeField] private TextMeshProUGUI playerStatusLabel = default;

    private UIState currentState;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Start()
    {
        SetNetworkStatus("connecting to server...");
        GoLobby();
    }

    public void Refresh()
    {
        SetUIState(currentState);
    }

    public void GoLobby()
    {
        SetUIState(UIState.Lobby);
    }

    public void GoHowToPlay()
    {
        SetUIState(UIState.HowToPlay);
    }

    public void SetUIState(UIState state)
    {
        TurnOffAll();
        currentState = state;
        switch (state)
        {
            case UIState.Lobby:
                warningDot.enabled = false;
                multiplayerButton.interactable = PhotonNetwork.IsConnectedAndReady;
                GameController.isSolo = false;
                GameController.goodGeneration = false;
                lobbyUI.SetActive(true);
                break;
            case UIState.HowToPlay:
                howtoplayUI.SetActive(true);
                break;
            case UIState.Shearch:
                searchUI.SetActive(true);
                break;
            case UIState.Room:
                roomUI.SetActive(true);
                var isHost = PhotonNetwork.IsMasterClient;
                var isReady = PhotonNetwork.PlayerList.Length == 2;
                hostUI.SetActive(isHost || GameController.isSolo);
                SetNetworkStatus(isReady ? "ready to start" : "waiting for a mate");
                if (GameController.isSolo) SetNetworkStatus("single player");
                startButton.gameObject.SetActive(!GameController.isSolo);
                startButton.interactable = isReady;
                soloButton.interactable = !isReady || GameController.isSolo;
                playerStatusLabel.text = GameController.isHomeless ? "I am Homeless" : "I am Passerby";
                break;
            case UIState.Game:
                SetNetworkStatus("waiting for all players...");
                gameUI.gameObject.SetActive(true);
                gameUI.Init();
                break;
            default:
                break;
        }
    }

    private void TurnOffAll()
    {
        lobbyUI.SetActive(false);
        howtoplayUI.SetActive(false);
        searchUI.SetActive(false);
        roomUI.SetActive(false);
        gameUI.gameObject.SetActive(false);
    }

    public void SetNetworkStatus(string value)
    {
        networkStatusLabel.text = value;
    }

    public void DropItems(TweenCallback callback)
    {
        gameUI.DropItems(callback);
    }

    public void FindItem(int id, TweenCallback<int> callback)
    {
        gameUI.FindItem(id, callback);
    }

    public void Warning()
    {
        warningDot.enabled = true;
    }

    public static void SetScore(int score)
    {
        instance.gameUI.SetScore(score);
    }

    public static void Hint(string hint, Role role)
    {
        instance.gameUI.ShowHint(hint, role);
    }

    public static void ShowFinal(int score)
    {
        instance.gameUI.ShowResult(score);
    }

    public void HardMode(bool isOn)
    {
        GameController.hardMode = isOn;
    }

    public void Exit()
    {
        Application.Quit();
    }
}

public enum UIState
{
    Lobby,
    HowToPlay,
    Shearch,
    Room,
    Game,
    Off
}

public enum Role
{
    Homeless,
    Passerby,
    Both
}
