﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public static bool hardMode = false;
    public static bool isSolo;
    public static bool isHomeless;
    public static bool isReady = false;
    public static bool homelessTurn = false;
    public static List<int> goalIDs = new List<int>();
    public static List<int> pocketIDs = new List<int>();
    public static List<int> sewerIDs = new List<int>();
    public static List<int> pocketReadyIDs = new List<int>();
    public static List<int> clueIDs = new List<int>();
    public static int turn = 0;

    public ItemList roster;

    public static bool goodGeneration = false;

    //[SerializeField] int goalCount = 7;
    [SerializeField] int sewerCount = 12;
    [SerializeField] int pocketCount = 16;
    [SerializeField] SessionController sessionController = default;

    private static bool isAnimating = false;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    public static void HostInit()
    {
        isHomeless = Random.Range(0f, 1f) > 0.5f;
        while (!goodGeneration) goodGeneration = instance.GenerateLists();
    }

    public static void GuestInit(int[] goal, int[] pocket, int[] sewer)
    {
        goalIDs = goal.ToList();
        pocketIDs = pocket.ToList();
        sewerIDs = sewer.ToList();

        SetReady();
    }

    public static void SwitchRoles()
    {
        isHomeless = !isHomeless;
    }

    public static void SetReady()
    {
        isAnimating = false;
        isReady = true;
        homelessTurn = false;
        turn = 0;
        pocketReadyIDs.Clear();
        clueIDs.Clear();
        UI.Hint("wait for clues from passerby", Role.Homeless);
        UI.Hint("choose two clues", Role.Passerby);
        UI.SetScore(0);
        UI.instance.SetNetworkStatus("");
        if (isSolo && isHomeless) AITurn();
    }

    private bool GenerateLists()
    {
        roster.Init();
        var allIDs = new List<int>();
        for (int i = 0; i < roster.items.Count; i++)
        {
            allIDs.Add(roster.items[i].id);
        }
        goalIDs.Clear();
        pocketIDs.Clear();
        sewerIDs.Clear();

        if (hardMode)
        {
            //TEMP SIMPLE
            while (goalIDs.Count < 7)
            {
                var id = allIDs[Random.Range(0, allIDs.Count)];
                goalIDs.Add(id);
                sewerIDs.Add(id);
                allIDs.Remove(id);
            }
            while (sewerIDs.Count < sewerCount)
            {
                var id = allIDs[Random.Range(0, allIDs.Count)];
                sewerIDs.Add(id);
                allIDs.Remove(id);
            }
            while (pocketIDs.Count < pocketCount)
            {
                var id = allIDs[Random.Range(0, allIDs.Count)];
                pocketIDs.Add(id);
                allIDs.Remove(id);
            }
        }
        else
        {
            //primary goal
            var primalGoal = allIDs[Random.Range(0, allIDs.Count)];
            allIDs.Remove(primalGoal);

            goalIDs.Add(primalGoal);
            sewerIDs.Add(primalGoal);

            //2 secondary goal from primary connects
            var secondaryGoals = new List<int>();
            var primalConnects = new List<int>();
            foreach (var item in roster.items[primalGoal].connects) primalConnects.Add(item.id);
            for (int i = 0; i < 2; i++)
            {
                var rndConnect = primalConnects[Random.Range(0, primalConnects.Count)];
                secondaryGoals.Add(rndConnect);
                primalConnects.Remove(rndConnect);
                allIDs.Remove(rndConnect);

                goalIDs.Add(rndConnect);
                sewerIDs.Add(rndConnect);
            }

            //4 last goals from secondaries
            var lastGoals = new List<int>();
            foreach (var id in secondaryGoals)
            {
                var secondaryConnects = new List<int>();
                foreach (var item in roster.items[id].connects) if (allIDs.Contains(item.id)) secondaryConnects.Add(item.id);
                if (secondaryConnects.Count < 2)
                {
                    Debug.LogWarning("less than 2 connects left for goal " + id);
                    //UI.instance.Warning();
                    return false;
                }
                for (int i = 0; i < 2; i++)
                {
                    var rndConnect = secondaryConnects.Count > 0 ? secondaryConnects[Random.Range(0, secondaryConnects.Count)] : allIDs[Random.Range(0, allIDs.Count)];
                    lastGoals.Add(rndConnect);
                    secondaryConnects.Remove(rndConnect);
                    allIDs.Remove(rndConnect);

                    goalIDs.Add(rndConnect);
                    sewerIDs.Add(rndConnect);
                }
            }

            //8 pocket as clues for last goals
            foreach (var id in lastGoals)
            {
                var lastConnects = new List<int>();
                foreach (var item in roster.items[id].connects) if (allIDs.Contains(item.id)) lastConnects.Add(item.id);
                if (lastConnects.Count < 2)
                {
                    Debug.LogWarning("less than 2 connects left for goal " + id);
                    //UI.instance.Warning();
                    return false;
                }
                for (int i = 0; i < 2; i++)
                {
                    var rndConnect = lastConnects.Count > 0 ? lastConnects[Random.Range(0, lastConnects.Count)] : allIDs[Random.Range(0, allIDs.Count)];
                    lastConnects.Remove(rndConnect);
                    allIDs.Remove(rndConnect);

                    pocketIDs.Add(rndConnect);
                }
            }

            //fill rest of sewer
            while (sewerIDs.Count < sewerCount)
            {
                var id = allIDs[Random.Range(0, allIDs.Count)];
                sewerIDs.Add(id);
                allIDs.Remove(id);
            }

            //fill rest of pocket
            while (pocketIDs.Count < pocketCount)
            {
                var id = allIDs[Random.Range(0, allIDs.Count)];
                pocketIDs.Add(id);
                allIDs.Remove(id);
            }
        }

        goalIDs = Shuffle(goalIDs);
        pocketIDs = Shuffle(pocketIDs);
        sewerIDs = Shuffle(sewerIDs);

        return true;
    }

    public static ItemState OnItemTap(int id)
    {
        if (!isReady) return ItemState.Skip;
        if (isAnimating) return ItemState.Skip;
        if (pocketIDs.Contains(id))
        {
            if (homelessTurn) Debug.LogError("non-active player input");
            pocketReadyIDs.Add(id);
            pocketIDs.Remove(id);
            if (pocketReadyIDs.Count == 2)
            {
                UI.Hint("wait for an item from Homeless", Role.Passerby);
                DropItems();
                return ItemState.Skip;
            }
            else
            {
                UI.Hint("choose one more clue", Role.Passerby);
                return ItemState.Chosen;
            }
        }
        else if (pocketReadyIDs.Contains(id))
        {
            if (homelessTurn) Debug.LogError("non-active player input");
            pocketReadyIDs.Remove(id);
            pocketIDs.Add(id);
            UI.Hint("choose two clues", Role.Passerby);
            return ItemState.Normal;
        }
        else if (sewerIDs.Contains(id))
        {
            if (!homelessTurn) Debug.LogError("non active player input");
            UI.Hint("wait for clues from Passerby", Role.Homeless);
            FindItem(id);
            return ItemState.Normal;
        }
        Debug.LogError("where should be " + id + " id?");
        return ItemState.Normal;
    }

    public static void DropItems()
    {
        isAnimating = true;
        UI.instance.DropItems(DropItemsEnd);
    }
    
    public static void DropItemsEnd()
    {
        isAnimating = false;
        if (!isHomeless && !isSolo) instance.sessionController.DropItems();
        clueIDs = new List<int>(pocketReadyIDs);
        pocketReadyIDs.Clear();
        homelessTurn = true;
        UI.Hint("choose an item based on clues", Role.Homeless);
        if (isSolo && !isHomeless) AITurn();
    }

    public static void FindItem(int id)
    {
        isAnimating = true;
        UI.instance.FindItem(id, FindItemEnd);
    }

    public static void FindItemEnd(int id)
    {
        isAnimating = false;
        if (isHomeless && !isSolo) instance.sessionController.FindItem(id);
        pocketIDs.Add(id);
        sewerIDs.Remove(id);
        goalIDs.Remove(id);
        clueIDs.Clear();
        homelessTurn = false;
        UI.Hint("choose two clues", Role.Passerby);
        turn++;
        UI.SetScore(7 - goalIDs.Count);
        if (turn >= 7)
        {
            isReady = false;
            //UI.Hint("game is over. your score is " + (7 - goalIDs.Count) + " / 7", Role.Both);
            UI.Hint("", Role.Both);
            UI.ShowFinal(7 - goalIDs.Count);
        }
        if (isSolo && isHomeless) AITurn();
    }

    private static List<int> Shuffle (List<int> list)
    {
        var shuffled = new List<int>();
        while (list.Count > 0)
        {
            var rnd = list[Random.Range(0, list.Count)];
            list.Remove(rnd);
            shuffled.Add(rnd);
        }
        return shuffled;
    }

    #region AI
    public static void AITurn()
    {
        //Debug.Log("AI TURN as " + (homelessTurn ? "Homeless" : "Passerby"));
        if (homelessTurn)
        {
            var whatIGot = Shuffle(new List<int>(sewerIDs));
            var bestMatches = -1;
            var bestID = 0;
            for (int i = 0; i < whatIGot.Count; i++)
            {
                var matches = 0;
                foreach (var clue in clueIDs)
                {
                    foreach (var connect in instance.roster.items[clue].connects)
                    {
                        if (connect.id == whatIGot[i]) matches++;
                    }
                }
                if (matches > bestMatches)
                {
                    bestMatches = matches;
                    bestID = whatIGot[i];
                }
            }
            if (!sewerIDs.Contains(bestID)) Debug.LogError("wrong id: " + bestID);
            if (pocketIDs.Contains(bestID)) Debug.LogError("id: " + bestID + " already in the pocket");
            OnItemTap(bestID);
        }
        else
        {
            while (pocketReadyIDs.Count > 0) OnItemTap(pocketReadyIDs[0]);
            
            var whatIGot = Shuffle(new List<int>(pocketIDs));
            var goalsLeft = Shuffle(new List<int>(goalIDs));
            var bestMatches = -1;
            var bestID_1 = 0;
            var bestID_2 = 0;
            for (int i = 0; i < whatIGot.Count; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    var goalMatches = -1;
                    var bestGoalID_1 = 0;
                    var bestGoalID_2 = 0;
                    for (int g = 0; g < goalIDs.Count; g++)
                    {
                        var matches = 0;
                        foreach (var connect in instance.roster.items[goalsLeft[g]].connects)
                        {
                            if (connect.id == whatIGot[i]) matches++;
                            if (connect.id == whatIGot[j]) matches++;
                        }
                        if (matches > goalMatches)
                        {
                            goalMatches = matches;
                            bestGoalID_1 = whatIGot[i];
                            bestGoalID_2 = whatIGot[j];
                        }
                    }
                    if (goalMatches > bestMatches)
                    {
                        bestMatches = goalMatches;
                        bestID_1 = bestGoalID_1;
                        bestID_2 = bestGoalID_2;
                    }
                }
            }
            OnItemTap(bestID_1);
            OnItemTap(bestID_2);
        }
    }
    #endregion

    //private static void CheckSame()
    //{
    //    for (int i = 0; i < pocketIDs.Count; i++)
    //    {
    //        for (int j = 0; j < sewerIDs.Count; j++)
    //        {
    //            if (pocketIDs[i] == sewerIDs[j])
    //            {
    //                Debug.LogError("SAME ITEM");
    //            }
    //        }
    //    }
    //}
}
