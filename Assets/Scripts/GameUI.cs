﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using TMPro;
using DG.Tweening;

public class GameUI : MonoBehaviour
{
    [SerializeField] GameObject gamePasserbyUI = default;
    [SerializeField] GameObject gameHomelessUI = default;
    [SerializeField] Transform goalsTransform = default;
    [SerializeField] Transform pocketTransform = default;
    [SerializeField] Transform sewerTransform = default;
    [SerializeField] Transform cluesTransform = default;
    [SerializeField] TextMeshProUGUI scoreLabelHomeless = default;
    [SerializeField] TextMeshProUGUI scoreLabelPasserby = default;
    [SerializeField] TextMeshProUGUI hintLineHomeless = default;
    [SerializeField] TextMeshProUGUI hintLinePasserby = default;
    [SerializeField] RectTransform rackHomeless = default;
    [SerializeField] RectTransform rackPasserby = default;
    [SerializeField] GameObject finalScreen = default;
    [SerializeField] TextMeshProUGUI finalText = default;

    private ItemView[] goalItems;
    private ItemView[] pocketItems;
    private ItemView[] sewerItems;
    private ItemView[] clueItems;

    public void Init()
    {
        gameHomelessUI.SetActive(GameController.isHomeless);
        gamePasserbyUI.SetActive(!GameController.isHomeless);
        finalScreen.SetActive(false);

        goalItems = goalsTransform.GetComponentsInChildren<ItemView>();
        pocketItems = pocketTransform.GetComponentsInChildren<ItemView>();
        sewerItems = sewerTransform.GetComponentsInChildren<ItemView>();
        clueItems = cluesTransform.GetComponentsInChildren<ItemView>();

        for (var i = 0; i < goalItems.Length; i++)
        {
            goalItems[i].Init(GameController.instance.roster.items[GameController.goalIDs[i]]);
        }
        for (var i = 0; i < pocketItems.Length; i++)
        {
            pocketItems[i].Init(GameController.instance.roster.items[GameController.pocketIDs[i]]);
        }
        for (var i = 0; i < sewerItems.Length; i++)
        {
            sewerItems[i].Init(GameController.instance.roster.items[GameController.sewerIDs[i]]);
        }
        for (var i = 0; i < clueItems.Length; i++)
        {
            clueItems[i].TurnOff();
        }
    }

    public void DropItems(TweenCallback callback)
    {
        if (GameController.isHomeless) FindClues(callback);
        else DropFromPocket(callback);
    }

    private void FindClues(TweenCallback callback)
    {
        var duration = 1f;

        for (int i = 0; i < 2; i++)
        {
            clueItems[i].Init(GameController.instance.roster.items[GameController.pocketReadyIDs[i]]);
            clueItems[i].pic.position = rackHomeless.position;
            clueItems[i].pic.DOAnchorPosX(0f, duration).SetEase(Ease.Linear);
            clueItems[i].pic.DOAnchorPosY(0f, duration).SetEase(Ease.InQuad);           
        }

        transform.DOScale(1f, duration).OnComplete(callback);
    }

    private void DropFromPocket(TweenCallback callback)
    {
        var duration = 1f;

        foreach(var item in pocketItems)
        {
            if (GameController.pocketReadyIDs.Contains(item.id))
            {
                item.HL.enabled = false;
                item.pic.DOMoveX(rackPasserby.position.x, duration).SetEase(Ease.Linear);
                item.pic.DOMoveY(rackPasserby.position.y, duration).SetEase(Ease.InQuad).OnComplete(() => item.TurnOff());              
            }
        }

        transform.DOScale(1f, duration).OnComplete(callback);
    }

    public void FindItem(int id, TweenCallback<int> callback)
    {
        if (GameController.isHomeless) SendItem(id, callback);
        else ReceiveItem(id, callback);
    }

    private void SendItem(int id, TweenCallback<int> callback)
    {
        var duration = 1f;

        for (var i = 0; i < clueItems.Length; i++)
        {
            //clueItems[i].img.DOFade(0f, duration * 0.5f).OnComplete(() => clueItems[i].TurnOff());
            clueItems[i].TurnOff();
        }
        foreach (var item in sewerItems)
        {
            if (item.id == id)
            {
                item.pic.DOMoveX(rackHomeless.position.x, duration).SetEase(Ease.Linear);
                item.pic.DOMoveY(rackHomeless.position.y, duration).SetEase(Ease.OutQuad).OnComplete(() => item.TurnOff());
            }
        }

        transform.DOScale(1f, duration).OnComplete(() => callback(id));
    }

    private void ReceiveItem(int id, TweenCallback<int> callback)
    {
        var duration = 1f;

        var emptyItems = new List<ItemView>();
        for (int i = 0; i < pocketItems.Length; i++)
        {
            if (pocketItems[i].isEmpty) emptyItems.Add(pocketItems[i]);
        }
        var item = emptyItems[Random.Range(0, emptyItems.Count)];
        item.Init(GameController.instance.roster.items[id]);
        item.pic.position = rackPasserby.position;
        item.pic.DOAnchorPosX(0f, duration).SetEase(Ease.Linear);
        item.pic.DOAnchorPosY(0f, duration).SetEase(Ease.OutQuad).OnComplete(() =>
        {
            foreach (var goal in goalItems)
            {
                if (goal.id == id)
                {
                    goal.matchEffect.Play();
                    goal.TurnOff();
                }
            }
        });

        transform.DOScale(1f, duration).OnComplete(() => callback(id));
    }

    public void SetScore(int score)
    {
        if (GameController.turn == 0)
        {
            scoreLabelHomeless.text = "";
            scoreLabelPasserby.text = "<size=50>the items at the top are your goal";
        }
        else
        {
            scoreLabelHomeless.text = score + " / " + GameController.turn;
            scoreLabelPasserby.text = score + " / " + GameController.turn;
        }
    }

    public void ShowHint(string hint, Role role)
    {
        if (role == Role.Both || role == Role.Homeless) hintLineHomeless.text = hint;
        if (role == Role.Both || role == Role.Passerby) hintLinePasserby.text = hint;
    }

    public void ShowResult(int score)
    {
        transform.DOScale(1f, 1f).OnComplete(() =>
         {
             finalScreen.SetActive(true);
             finalText.text = score + " items of 7 possible";
         });
    }
}
