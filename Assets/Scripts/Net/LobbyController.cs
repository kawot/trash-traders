﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyController : MonoBehaviourPunCallbacks
{
    //public override void OnEnable()
    //{
    //    PhotonNetwork.AddCallbackTarget(this);
    //}

    //public override void OnDisable()
    //{
    //    PhotonNetwork.RemoveCallbackTarget(this);
    //}

    public override void OnConnectedToMaster()
    {
        UI.instance.Refresh();
    }

    public void QuickJoin()
    {
        UI.instance.SetUIState(UIState.Shearch);
        UI.instance.SetNetworkStatus("looking for a game...");
        Debug.Log("quick search...");
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("failed to join room...");
        CreateRoom();
    }

    private void CreateRoom()
    {
        Debug.Log("creating room...");
        var roomNumber = Random.Range(0, 1000);
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 2 };
        PhotonNetwork.CreateRoom("Room " + roomNumber, roomOptions);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("failed to create room...");
        CreateRoom(); 
    }

    public void SinglePlayer()
    {
        GameController.isSolo = true;
        GameController.HostInit();
        UI.instance.SetUIState(UIState.Room);
    }
}
