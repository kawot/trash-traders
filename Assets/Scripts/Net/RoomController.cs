﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomController : MonoBehaviourPunCallbacks
{
    private bool leaveFlag = false;

    //public override void OnEnable()
    //{
    //    PhotonNetwork.AddCallbackTarget(this);
    //}

    //public override void OnDisable()
    //{
    //    PhotonNetwork.RemoveCallbackTarget(this);
    //}

    public override void OnJoinedRoom()
    {
        if (leaveFlag)
        {
            leaveFlag = false;
            LeaveRoom();
        }
        Debug.Log("joined room");
        if (PhotonNetwork.IsMasterClient) GameController.HostInit();
        UI.instance.SetUIState(UIState.Room);
    }

    public void LeaveRoom()
    {
        if (PhotonNetwork.InRoom) PhotonNetwork.LeaveRoom();
        else if (!GameController.isSolo) leaveFlag = true;
        if (GameController.isSolo) OnLeftRoom();
    }

    public override void OnLeftRoom()
    {
        UI.instance.GoLobby();
        //SceneManager.LoadScene(0);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        UI.instance.Refresh();
        if (PhotonNetwork.IsMasterClient) photonView.RPC("RPC_SendRole", RpcTarget.Others, GameController.isHomeless);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        if (GameController.isReady)
        {
            GameController.isSolo = true;
            if (GameController.isHomeless != GameController.homelessTurn) GameController.AITurn();
            UI.instance.SetNetworkStatus("player left");
        }
        else UI.instance.Refresh();
    }

    public void Switch()
    {
        GameController.SwitchRoles();
        UI.instance.Refresh();
        if (!GameController.isSolo) photonView.RPC("RPC_SendRole", RpcTarget.Others, GameController.isHomeless);
    }

    public void StartMultiplayer()
    {
        PhotonNetwork.CurrentRoom.IsOpen = false;
        GameController.isSolo = false;
        UI.instance.SetUIState(UIState.Game);
        photonView.RPC("RPC_StartGame", RpcTarget.Others, GameController.goalIDs.ToArray(), GameController.pocketIDs.ToArray(), GameController.sewerIDs.ToArray());
    }

    public void StartSolo()
    {
        GameController.isSolo = true;
        UI.instance.SetUIState(UIState.Game);
        GameController.SetReady();
    }

    [PunRPC]
    private void RPC_SendRole(bool masterIsHomeless)
    {
        GameController.isHomeless = !masterIsHomeless;
        UI.instance.Refresh();
    }

    [PunRPC]
    private void RPC_StartGame(int[] goal, int[] pocket, int[] sewer)
    {        
        GameController.GuestInit(goal, pocket, sewer);
        UI.instance.SetUIState(UIState.Game);
        UI.instance.SetNetworkStatus("");
        photonView.RPC("RPC_Ready", RpcTarget.MasterClient);
    }

    [PunRPC]
    private void RPC_Ready()
    {
        GameController.SetReady();
    }
}
