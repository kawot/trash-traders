﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionController : MonoBehaviourPunCallbacks
{
    public void DropItems()
    {
        photonView.RPC("RPC_DropItems", RpcTarget.Others, GameController.pocketReadyIDs.ToArray());
    }

    [PunRPC]
    private void RPC_DropItems(int[] drops)
    {
        for (int i = 0; i < drops.Length; i++)
        {
            GameController.OnItemTap(drops[i]);
        }
    }

    public void FindItem(int id)
    {
        photonView.RPC("RPC_FindItem", RpcTarget.Others, id);
    }

    [PunRPC]
    private void RPC_FindItem(int id)
    {
        GameController.OnItemTap(id);
    }
}
