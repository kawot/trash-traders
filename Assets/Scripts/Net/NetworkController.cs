﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class NetworkController : MonoBehaviourPunCallbacks
{

    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();   
    }

    public override void OnConnectedToMaster()
    {
        UI.instance.SetNetworkStatus("Connected to " + PhotonNetwork.CloudRegion + " server");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        UI.instance.Refresh();
        //CHECK FOR ALL SITUATIONS
    }
}
