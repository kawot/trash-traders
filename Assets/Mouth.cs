﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mouth : MonoBehaviour
{
    public Sprite[] sprites;
    public Image image;
    public float time;

    private void OnEnable()
    {
        StartCoroutine(ChangeSprite());
    }

    IEnumerator ChangeSprite()
    {
        while (time > 0.01f)
        {
            yield return new WaitForSeconds(time);
            image.sprite = sprites[Random.Range(0, sprites.Length)];

        }
    }
}
